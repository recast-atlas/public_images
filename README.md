# public_images

This project is dedicated to containing ATLAS images which have been approved to be made public. All images stored in the [container registry](./container_registry) can be downloaded without any CERN credentials.

## Description of Images in Container Registry

### 1. Light flavour calibration based on Z+jet events

**Command to pull image:** `docker pull gitlab-registry.cern.ch/recast-atlas/public_images/ljets_zjet:AnalyseNtuple-master-d2c9fd43`

**Link to original gitlab project:** https://gitlab.cern.ch/atlas-ftag-calibration/ljets_ZJet

**Contact person:** Laura Barranco (lbarranc@cern.ch)
